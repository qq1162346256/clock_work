﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour {

    [SerializeField] float force;
    [SerializeField] AudioClip bounceDuang;

    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision other)
    {
        //check if the collided object is a delivery
        if (other.gameObject.tag == "Delivery")
        {
            //add a force to the palyer object
            other.gameObject.GetComponent<Rigidbody>().AddForce(force * transform.up);

            //play bouncing effect
            source.PlayOneShot(bounceDuang);
        }
    }

}
