﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//prevent multiple oscillator on a single object
[DisallowMultipleComponent]

public class Oscillator : MonoBehaviour {

    //design levers
    [SerializeField] Vector3 movementVector;
    [Range(0, 1)] [SerializeField] float movementFactor;
    [SerializeField] float period = 2f;

    //data references
    Vector3 startPos;
    private bool isMove = true;

    // Use this for initialization
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (isMove)
        {
            AutoMovement();
        }
    }

    private void AutoMovement()
    {
        //set movementFactor automatically
        if (period <= Mathf.Epsilon)
        {
            return;
        }

        //using time with sine function to move the object
        float cycles = Time.time / period;
        const float tau = Mathf.PI * 2;
        float rawSinWave = Mathf.Sin(tau * cycles);
        movementFactor = rawSinWave / 2 + 0.5f;

        transform.position = movementVector * movementFactor + startPos;
    }

    //getter for period
    public float GetPeriod()
    {
        return period;
    }

    //setter for period
    public void SetPeriod(float rePeriod)
    {
        if (rePeriod > 0)
        {
            period = rePeriod;
        }
    }

    //when adjusted by clockwork, stop moving
    public void PauseOscillation()
    {
        isMove = false;
    }

    //whennot adjusted, continue moving
    public void ContinueOscillation()
    {
        isMove = true;
    }
}
