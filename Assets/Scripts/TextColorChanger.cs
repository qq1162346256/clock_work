﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextColorChanger : MonoBehaviour {

    //design levers
    [SerializeField] TMP_Text targetText;
    [SerializeField] float colorChangingRate = 1.0f;

    //private fields
    private Color targetColor;
    private float timeLeft;

	// Use this for initialization
	void Start () {
        targetText.outlineColor = Color.red;
	}
	
	// Update is called once per frame
	void Update ()
    {
        ChangeColor();
    }

    private void ChangeColor()
    {
        //check if a transaction is complete
        if (timeLeft <= Time.deltaTime)
        {
            //if the current target color is achieved, change to another one
            targetText.outlineColor = targetColor;

            //make a new target color to change to
            targetColor = new Color(Random.value, Random.value, Random.value);
            timeLeft = colorChangingRate;
        }
        else
        {
            //in mid way of transaction to the target color
            targetText.outlineColor = Color.Lerp(targetText.outlineColor, targetColor, Time.deltaTime / timeLeft);

            // update the timer
            timeLeft -= Time.deltaTime;
        }
    }
}
