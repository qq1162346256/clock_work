﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTweaker : MonoBehaviour {

    //design levers
    [SerializeField] GameObject clockWorker;

    //private fields
    //bool onclick = false;
    ClockWorker cw;
    LevelManager lm;
    private const float ROTATION_MULTIPLIER = 100.0f;
    private const int ORIEN_POS = 1;
    private const int ORIEN_NEG = -1;

	// Use this for initialization
	void Start () {
        //get the clockworker the tweaker corresponds to
        cw = clockWorker.GetComponent<ClockWorker>();

        //get the level manager of the current level
        lm = FindObjectOfType<LevelManager>();
	}

    private void OnMouseOver()
    {
        //detect if the user pressed left mouse button or right mouse button
        if (Input.GetMouseButton(0))
        {
            //if the right mouse button is held, tweak the clock work on positive side
            Debug.Log("Left Mouse Button Held");

            //check if the positive tweak is successful
            if (cw.TweakPositive())
            {
                //set tweaking
                cw.Tweaking(true);
                SpinOnClick(ORIEN_POS);
            }
            else
            {
                //set not tweaking
                cw.Tweaking(false);
            }
        }
        else if (Input.GetMouseButton(1))
        {
            //if the right mouse button is held, tweak the clock work on negative side
            Debug.Log("Right Mouse Button Held");

            //check if the negative tweak is successful
            if (cw.TweakNegative())
            {
                //set tweaking
                cw.Tweaking(true);
                SpinOnClick(ORIEN_NEG);
            }
            else
            {
                //set not tweaking
                cw.Tweaking(false);
            }
        }
        else
        {
            cw.Tweaking(false);
        }
    }

    //when mouse is no longer on the object, set tweaking to false
    private void OnMouseExit()
    {
        cw.Tweaking(false);
    }

    private void SpinOnClick(int orien)
    {
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATION_MULTIPLIER * orien);
    }
}
