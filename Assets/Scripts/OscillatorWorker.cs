﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OscillatorWorker : ClockWorker {

    //design levers
    [SerializeField] float periodChangeBound;
    [SerializeField] float rateChange;

    //private fields
    private Oscillator oscillator;
    private float period;
    private float originalPeriod;
    //private int sign = 1;

	// Use this for initialization
	void Start () {
        oscillator = GetComponent<Oscillator>();
        period = oscillator.GetPeriod();
        originalPeriod = period;
	}
	
	// Update is called once per frame
	void Update () {
        if (!GetTweak())
        {
            oscillator.ContinueOscillation();
        }
    }

    public override bool TweakPositive()
    {
        //check if the period is beyond the bound
        if (period >= originalPeriod + (periodChangeBound / 2))
        {
            //set period to its upbound
            period = originalPeriod + (periodChangeBound / 2);
            return false;
        }

        //if its not beyond up bound, set new period
        period = period + (Time.deltaTime * rateChange);

        //pause the movement of oscillator
        oscillator.PauseOscillation();

        //set the new period to oscillator
        oscillator.SetPeriod(period);

        return true;
    }

    public override bool TweakNegative()
    {
        //check if the period is below the bound
        if (period <= originalPeriod - (periodChangeBound / 2))
        {
            //set period to its upbound
            period = originalPeriod - (periodChangeBound / 2);
            return false;
        }

        //if its not beyond up bound, set new period
        period = period - (Time.deltaTime * rateChange);

        //pause the movement of oscillator
        oscillator.PauseOscillation();

        //set the new period to oscillator
        oscillator.SetPeriod(period);

        return true;
    }
}
