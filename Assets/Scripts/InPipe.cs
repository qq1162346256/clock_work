﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InPipe : MonoBehaviour {

    //private fields
    private LevelManager lm;
    private const float DESTROY_DELAY = 0.35f;

	// Use this for initialization
	void Start () {

        //get the level manager
        lm = FindObjectOfType<LevelManager>();

	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Delivery")
        {
            //set the delivery to state delivered
            other.gameObject.GetComponent<Delivery>().NotifyDelivery();

            //destroy the delivery and increase point
            Destroy(other.gameObject, DESTROY_DELAY);
            lm.IncreasePoint();
        }
    }
}
