﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resizer : MonoBehaviour {

    //design levers
    [SerializeField] float resizeScale = 1;
    [SerializeField] float rateOfRotation;
    [SerializeField] AudioClip transSmall;
    [SerializeField] AudioClip transBig;

    //private fields
    private Vector3 facing;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        facing = transform.up;
        source = GetComponent<AudioSource>();
	}

    private void FixedUpdate()
    {
        SelfRotate();
    }

    //here is a function to rotate the resizer every frame
    void SelfRotate()
    {
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * rateOfRotation);
    }

    //to detect other object to trigger the resizer
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Delivery")
        {

            Vector3 contactVelocity = other.GetComponent<Rigidbody>().velocity.normalized;
            facing = transform.up;

            if (resizeScale != 0)
            {
                if (Vector3.Angle(contactVelocity, facing) > 90)
                {
                    //big it
                    Debug.Log("big");
                    other.gameObject.transform.localScale = resizeScale * other.gameObject.transform.localScale;

                    //playe the audio effect
                    source.PlayOneShot(transBig);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Delivery")
        {

            Vector3 contactVelocity = other.GetComponent<Rigidbody>().velocity.normalized;

            if (resizeScale != 0)
            {
                if (Vector3.Angle(contactVelocity, facing) < 90)
                {
                    //small it
                    Debug.Log("small");
                    other.gameObject.transform.localScale = (1 / resizeScale) * other.gameObject.transform.localScale;

                    //play the audio effect
                    source.PlayOneShot(transSmall);
                }
            }
        }
    }
}
