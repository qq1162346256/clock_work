﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlowWorker : ClockWorker {

    //design levers
    [SerializeField] float windUpBound;
    [SerializeField] float windLowBound;
    [SerializeField] float rate = 20.0f;

    //private fields
    private WindBlower blower;
    private float level;
    private float originalLevel;

    // Use this for initialization
    void Start () {
        blower = gameObject.GetComponentInChildren<WindBlower>();
        level = blower.GetWindLevel();
        originalLevel = level;
	}

    public override bool TweakNegative()
    {
        level -= rate * Time.deltaTime;

        //check if level is below low bound
        if(level < (originalLevel + windLowBound))
        {
            //regulate level and return false
            level = originalLevel + windLowBound;
            return false;
        }

        //if everything's fine, set the new wind level
        blower.SetWindLevel(level);
        return true;
    }

    public override bool TweakPositive()
    {
        level += rate * Time.deltaTime;

        //check if level is beyond up bound
        if (level > (originalLevel + windUpBound))
        {
            //regulate level and return false
            level = originalLevel + windUpBound;
            return false;
        }

        //if everything's fine, set the new wind level
        blower.SetWindLevel(level);
        return true;
    }
}
